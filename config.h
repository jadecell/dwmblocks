// Modify this file to change what commands output to your statusbar, and
// recompile using the make command.
static const Block blocks[] = {
    /*Icon*/ /*Command*/ /*Update Interval*/ /*Update Signal*/

    {"", "sb-cpuperc nerd", 5, 9},
    {"", "sb-memory nerd", 5, 7},
    {"", "sb-cputemp", 5, 4},
    {"", "sb-portpacks", 3600, 5},
    {"", "sb-clock-nerd", 1, 2},

};

// sets delimeter between status commands. NULL character ('\0') means no
// delimeter.
static char *delim = " | ";

// Sets delimiters around the full statusbar. NULL character ('\0') means no
// delimeter.
static char leftpad[] = " ";
static char rightpad[] = " ";
